﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Editor2D
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void triangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var shape = new TShape();
            shape.Vertices.Add(new PointF(-1, -1));
            shape.Vertices.Add(new PointF(1, -1));
            shape.Vertices.Add(new PointF(0, 1));
            shape.Scale = new PointF(50, 50);
            shape.Rotation = 10;
            shape.Origin = new PointF(200, 150);
            shape.Parent = tShapeView1.Root;
            tShapeView1.Invalidate();
        }

        private void rectangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var shape = new TRectangle();
            shape.Scale = new PointF(50, 100);
            shape.Rotation = 45;
            shape.Origin = new PointF(100, 250);
            shape.Parent = tShapeView1.Root;
            tShapeView1.Invalidate();
        }

        private void ellipseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var shape = new TElipse();
            shape.Scale = new PointF(50, 100);
            shape.Rotation = 75;
            shape.Origin = new PointF(200, 250);
            shape.Parent = tShapeView1.Root;
            tShapeView1.Invalidate();
        }
    }
}
