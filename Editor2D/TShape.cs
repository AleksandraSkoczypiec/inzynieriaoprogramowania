﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor2D
{
    public class TShape
    {
        public Color ForeColor = Color.Black;
        public Color BackColor = Color.Blue;
        public List<TShape> Children = new List<TShape>();
        TShape FParent;
        public TShape Parent
        {
            get
            {
                return FParent;
            }
            set
            {
                if (FParent != null)
                {
                    FParent.Children.Remove(this);
                }

                FParent = value;
                if (FParent != null)
                {
                    FParent.Children.Add(this);
                }
            }
        }
        public List<PointF> Vertices = new List<PointF>();
        protected virtual void DrawShape(Graphics gc)
        {
            if (Vertices.Count > 0)
            {
                var brush = new SolidBrush(BackColor);
                var pts = Vertices.ToArray();
                gc.FillPolygon(brush, pts);
            }
            var pen = new Pen(ForeColor);
            pen.Width = 0;
            gc.DrawRectangle(pen, Rectangle.Round(BBox));
        }
        public void Draw(Graphics gc)
        {
            var currentTransform = gc.Transform;
            gc.MultiplyTransform(Transform);
            DrawShape(gc);
            for(int i = 0; i < Children.Count; i++)
            {
                Children[i].Draw(gc);
            }
            gc.Transform = currentTransform;
        }
        public PointF TransformPoint(PointF p)
        {
            var pts = new PointF[] { p };
            Transform.TransformPoints(pts);
            return pts[0];
        }
        public RectangleF BBox
        {
            get
            {
                var bbox = RectangleF.Empty;
                for(int i =0; i < Vertices.Count; i++)
                {
                    var rc = new RectangleF(Vertices[i], SizeF.Empty);
                    if (i == 0)
                    {
                        bbox = rc;
                    }
                    else
                    {
                        bbox = RectangleF.Union(bbox, rc);
                    }
                }
                bool needInit = Vertices.Count == 0;
                for (int i = 0; i < Children.Count; i++)
                {
                    var child = Children[i];
                    var childBBox = child.BBox;
                    var tl = childBBox.Location;
                    var br = tl + childBBox.Size;
                    for(int j=0; j<4; j++)
                    {
                        var p = tl;
                        if ((j & 1) != 0) p.X = br.X;
                        if ((j & 2) != 0) p.Y = br.Y;

                        p = child.TransformPoint(p);
                        var rc = new RectangleF(p, SizeF.Empty);
                        if(needInit)
                        {
                            bbox = rc;
                            needInit = false;
                        }
                        else
                        {
                            bbox = RectangleF.Union(bbox, rc);
                        }
                    }
                }
                return bbox;
            }
        }
        public PointF Scale = new PointF(1, 1);
        public float Shear;
        public float Rotation;
        public PointF Origin;
        public Matrix Transform
        {
            get
            {
                var transform = new Matrix();
                transform.Translate(Origin.X, Origin.Y);
                transform.Rotate(Rotation);
                transform.Shear(Shear, 0);
                transform.Scale(Scale.X, Scale.Y);
                return transform;
            }
        }


    }
}
