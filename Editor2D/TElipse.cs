﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor2D
{
    class TElipse : TShape
    {
        public TElipse()
        {
            Vertices.Add(new PointF(-1, -1));
            Vertices.Add(new PointF(1, 1));
        }

        protected override void DrawShape(Graphics gc)
        {
            var brush = new SolidBrush(BackColor);
            var el = new Rectangle(-1, -1, 2, 2);
            gc.FillEllipse(brush, el);
            var pen = new Pen(ForeColor);
            pen.Width = 0;
            gc.DrawRectangle(pen, Rectangle.Round(BBox));
        }
    }
}
